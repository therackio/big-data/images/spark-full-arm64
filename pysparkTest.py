# https://www.roseindia.net/bigdata/pyspark/pyspark-hello-world.shtml
# https://www.roseindia.net/bigdata/pyspark/NameError-name-sc-is-not-defined.shtml
from pyspark.sql import SparkSession
from pyspark import SparkContext
from operator import add

sparkSession = (SparkSession.builder.appName("SparkSessionExample").enableHiveSupport().getOrCreate())
sc = SparkContext.getOrCreate()
 
data = sc.parallelize(list("Hello World"))
counts = data.map(lambda x: 
	(x, 1)).reduceByKey(add).sortBy(lambda x: x[1],
	 ascending=False).collect()

for (word, count) in counts:
    print("{}: {}".format(word, count))

# print("Listing JARS...")
# jars = sc._jsc.sc().listJars().toArray()
# print(*jars)
