ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG SPARK_VERSION
ENV SPARK_VERSION $SPARK_VERSION

ARG HADOOP_VERSION
ENV HADOOP_VERSION $HADOOP_VERSION

# ARG HUDI_VERSION
# ENV HUDI_VERSION $HUDI_VERSION

# ARG PRESTODB_VERSION
# ENV PRESTODB_VERSION $PRESTODB_VERSION

# ARG AWS_GLUE_DATA_CATALOG_CLIENT_VERSION
# ENV AWS_GLUE_DATA_CATALOG_CLIENT_VERSION $AWS_GLUE_DATA_CATALOG_CLIENT_VERSION

# ARG AWS_SDK_JAVA_VERSION
# ENV AWS_SDK_JAVA_VERSION $AWS_SDK_JAVA_VERSION

LABEL maintainer="gitlab@therack.io"

# Setup + Configure Python
RUN \
    apt -qq -y update && \
    dpkg-reconfigure locales && \
    apt install -qq -y python3 python3-setuptools python3-pip procps && \
    python3 --version && \
    pip3 --version && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3 10 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10 && \
    python --version && \
    pip --version && \
    mkdir /jars

# Install Hadoop 
ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/apache-hadoop-bin-arm64/$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz /
RUN \
    cd / && \
    tar -xzf /hadoop-$HADOOP_VERSION.tar.gz && \
    rm -rf /hadoop-$HADOOP_VERSION.tar.gz && \
    ls -al && \
    ls -al /hadoop-$HADOOP_VERSION/lib/ && \
    ls -al /hadoop-$HADOOP_VERSION/lib/native

ENV HADOOP_HOME="/hadoop-$HADOOP_VERSION"
ENV HADOOP_OPTS="${HADOOP_OPTS} -Djava.library.path=${HADOOP_HOME}/lib/native"
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HADOOP_HOME}/lib/native"
ENV PATH="${HADOOP_HOME}/bin:${HADOOP_HOME}/sbin:${PATH}"

RUN \
    ldconfig && \
    hadoop version && \
    hadoop classpath

# Install Hive 
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/apache-hive-bin-arm64/$HIVE_VERSION/apache-hive-$HIVE_VERSION.tar.gz /
# RUN \
#    cd / && \
#    tar -xzf /apache-hive-$HIVE_VERSION.tar.gz && \
#    rm -rf /apache-hive-$HIVE_VERSION.tar.gz && \
#    ls -al /apache-hive-$HIVE_VERSION-bin && \
#    ls -al /apache-hive-$HIVE_VERSION-bin/lib && \
#    ls -al /apache-hive-$HIVE_VERSION-bin/bin && \
#    ls -al /apache-hive-$HIVE_VERSION-bin/jdbc

# ENV HIVE_HOME="/apache-hive-$HIVE_VERSION-bin"
# ENV PATH="${HIVE_HOME}/bin:${PATH}"
# ENV CLASSPATH="${CLASSPATH}:${HADOOP_HOME}/lib/*:${HIVE_HOME}/lib/*"
# ENV HIVE_CLASSPATH="${HIVE_HOME}/lib/*"

# RUN hive --version

# Add additional JARS
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/apache-hudi-bin-arm64/2.12-$HUDI_VERSION/hudi-spark-bundle_2.12-$HUDI_VERSION.jar /jars
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/prestodb-bin-arm64/$PRESTODB_VERSION/presto-cli-$PRESTODB_VERSION-executable.jar /jars
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/aws-sdk-java-arm64/$AWS_SDK_JAVA_VERSION/aws-java-sdk-bundle-$AWS_SDK_JAVA_VERSION.jar /jars
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/aws-glue-data-catalog-client-for-apache-hive-metastore-bin-arm64/$AWS_GLUE_DATA_CATALOG_CLIENT_VERSION/aws-glue-datacatalog-spark-client-$AWS_GLUE_DATA_CATALOG_CLIENT_VERSION.jar /jars
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/aws-glue-data-catalog-client-for-apache-hive-metastore-bin-arm64/$AWS_GLUE_DATA_CATALOG_CLIENT_VERSION/aws-glue-datacatalog-hive2-client-$AWS_GLUE_DATA_CATALOG_CLIENT_VERSION.jar /jars
# ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/aws-glue-data-catalog-client-for-apache-hive-metastore-bin-arm64/$AWS_GLUE_DATA_CATALOG_CLIENT_VERSION/aws-glue-datacatalog-client-common-$AWS_GLUE_DATA_CATALOG_CLIENT_VERSION.jar /jars

# Install Spark
ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/apache-spark-bin-arm64/$SPARK_VERSION/spark-$SPARK_VERSION-bin-without-hadoop-scala-2.12.tgz /
RUN \
    tar -xzf /spark-$SPARK_VERSION-bin-without-hadoop-scala-2.12.tgz && \
    rm -rf /spark-$SPARK_VERSION-bin-without-hadoop-scala-2.12.tgz && \
    ls -al && \
    cd /spark-$SPARK_VERSION-bin-without-hadoop-scala-2.12/python && \
    python3 setup.py sdist

ENV PYSPARK_PYTHON=python3
ENV SPARK_HOME="/spark-$SPARK_VERSION-bin-without-hadoop-scala-2.12"
ENV PATH="${SPARK_HOME}/bin:${SPARK_HOME}/sbin:${PATH}"

# Configure and Test Spark 
COPY ./pysparkTest.py /tmp/pysparkTest.py

RUN \
    touch $SPARK_HOME/conf/spark-env.sh && \
    echo "export SPARK_DIST_CLASSPATH=$(hadoop classpath)" >> $SPARK_HOME/conf/spark-env.sh && \
    cat $SPARK_HOME/conf/spark-env.sh && \
    chmod +x $SPARK_HOME/conf/spark-env.sh && \
    pyspark --version && \
    spark-submit --version && \
    chmod +x /tmp/pysparkTest.py && \
    spark-submit /tmp/pysparkTest.py && \
    rm -rf /tmp/pysparkTest.py
